package mypackage;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Test
 */
@WebServlet("/Test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("<<<<<<<<<<<<<<In Get on Jboss server >>>>>>>>>>>>");
		
		StringBuffer sb =  new StringBuffer();
		
		
		//this.mymain(sb);	
		
		this.mymain1(sb);	

		
		response.getWriter().append("Served at: ").append(request.getContextPath()).append(sb.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}
	
	public void mymain1(StringBuffer sb) { 
		
		System.out.println("mymain1>>>>>>>>>>>>>>>>>>>>>>>>>>>>>https://dpower.gslb.uatpan.bns:6212/CIFCostaRicaPanama/CustomerSearch?wsdl");
		
         try {  
        	 
        	 URL url = new URL("http://172.25.6.21:6429/cif/BranchList?wsdl");
        	 System.out.println("<<<<<<<<Hello World");
        	 //URL url = new URL("https://172.25.6.24:6843/CIFServiceWeb/CustomerLogUpd001?wsdl");
        	 //URL url = new URL("https://dpower.gslb.uatpan.bns:6212/CIFCostaRicaPanama/CustomerSearch?wsdl");
    		 URLConnection urlConnection = url.openConnection();
    		 InputStream in = urlConnection.getInputStream();
              InputStreamReader inputstreamreader = new InputStreamReader(in);  
              BufferedReader bufferedreader = new BufferedReader(inputstreamreader);  
              String string = null;  
              while ((string = bufferedreader.readLine()) != null) {  
                System.out.println("Received " + string+"\n\n\r"); 
                Date d = new Date();
                sb.append(d.getDate()+"\n\n\r"+string);
              }  
         } catch (MalformedURLException e) {  
              e.printStackTrace();  
         } catch (IOException e) {  
              e.printStackTrace();  
         }  
		
	}
	


}
